import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  const [user, setUser] = useState(null);

  const login = () => {
    // Simulez la connexion de l'utilisateur et obtenez les informations de l'utilisateur
    const loggedInUser = {
      username: "JohnDoe",
      // ... d'autres informations d'utilisateur
    };

    // Mettez à jour l'état de l'utilisateur dans App
    setUser(loggedInUser);
  };

  // Fonction pour simuler la déconnexion de l'utilisateur
  const logout = () => {
    setUser(null);
  };

  return (
    <div>
      <button onClick={login}>Log In</button>
      <button onClick={logout}>Log Out</button>

      <div className="App">
        <Navigation user={user} />
        <Profile user={user} setUser={setUser} />
      </div>
    </div>
  );
}

export default App;
