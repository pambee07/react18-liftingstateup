import "./Navigation.css";

function Navigation({ user }) {
  //  get user from props drilling
  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/*  check if user is connected */}
      <li>{user ? `Welcome, ${user.name}` : 'Please log in'}</li>
    </ul>
  );
}

export default Navigation;
